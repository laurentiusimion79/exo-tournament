package beans;

public class Swordsman {

	public int hitPoints;

	/**
	 * A Swordsman has 100 hit points and use a 1 hand sword that does 5 dmg
	 * A Viking has 120 hit points and use a 1 hand axe that does 6 dmg
	 */
	public void engage(Viking viking){
		int swordsmanLife = 100;
		int vikingLife = 120;

		while(swordsmanLife >= 0 && vikingLife >= 0){
			swordsmanLife -= 6;
			vikingLife -= 5;
		}

		if(swordsmanLife < 0 ){
			swordsmanLife = 0;
		}
		if(vikingLife < 0 ){
			vikingLife = 0;
		}

		setHitPoints(swordsmanLife);
		viking.setHitPoints(vikingLife);
	}

	public int hitPoints() {
		return hitPoints;
	}

	public void setHitPoints(int hitPoints) {
		this.hitPoints = hitPoints;
	}
}
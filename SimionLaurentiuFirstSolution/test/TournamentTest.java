package test;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

import beans.Swordsman;
import beans.Viking;

public class TournamentTest {
    @Test
    public void SwordsmanVsViking() {


        Swordsman swordsman = new Swordsman();

        Viking viking = new Viking();

        swordsman.engage(viking);

        assertThat(swordsman.hitPoints()).isEqualTo(0);
        assertThat(viking.hitPoints()).isEqualTo(35);

    }
}